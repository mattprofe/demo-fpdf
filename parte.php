<?php
	
	// incluimos la libreria que genera los pdf
	require ('fpdf/fpdf.php');

	// levantamos el json y lo convertimos en una matriz
	$listado = json_decode(file_get_contents("alumnos.json",true));

	// cambiamos la zona horaria
	ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

	// obtenemos la fecha y hora
	$time = date('d/m/y', time());

	// instanciamos la clase FPDF, se setea las medidas y el tamaño de la hoja
	$pdf = new FPDF("P", "mm", "Letter");

	// se añade una hoja
	$pdf->AddPage();
	// se añade una imagen
	$pdf->Image('img/logo-colegio.png',5,5,20,20,'png');
	// seteamos la fuente, su modo y el tamaño
	$pdf->SetFont("Arial", "B", 30);

	// Escribimos el pdf en una ubicación especifica "C" es la alineacion
	$pdf->Cell(200,40, "Listado de 71",0,1,"C");

	// Cambiamos el tamaño de la fuente
	$pdf->SetFont("Arial", "B", 14);

	// Escribimos en el pdf 
	$pdf->Cell(370,-70, 'Fecha: '.$time,0,1,"C");

	// Insertamos un salto de linea
	$pdf->Ln(63);

	// Se construye una tabla, y se escriben los encabezados, "C" especifica la alineacion
	$pdf->Cell(20,5,"Carnet",1,0,"C");
	$pdf->Cell(18,5,"Grupo",1,0,"C");
	$pdf->Cell(80,5,"Nombre",1,0,"C");

	// Cambiamos la fuente
	$pdf->SetFont("Arial", "", 10);

	// recorremos el la lista
	foreach ($listado as $key => $row) {
		// insertamos un salto de linea
		$pdf->Ln();
		// Se crean las celdas con los datos de un alumno
		$pdf->Cell(20,6, $row->carnet,1,0,"C");
		$pdf->Cell(18,6, $row->grupo,1,0,"C");
		$pdf->Cell(80,6, $row->apodo,1,0,"L");
	}

	// genero el pdf
	$pdf->Output();
	
?>