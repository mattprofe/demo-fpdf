DEMO-fpdf
=========
Ejemplo de uso de la libreria fpdf

Se utiliza para su desarollo y pruebas:

- PHP

Temas que se aplican
====================

- Lógica de programación PHP y buenas prácticas.
- Uso de libreria fpdf.

Uso
===

- Colocar el proyecto dentro del servidor y descomprimirlo.
- Acceder con un navegador a la carpeta demo-fpdf-master.
- Para ver el PDF generado por fpdf abrir parte.php.

Contenido
=========
- alumnos.json: Contiene un listado de alumnos json, con el siguiente formato:
[{"carnet":"11965","grupo":"1","apodo":"ARAGON TOBIAS GABRIEL"},...]
- parte.php: Dentro contiene el código que genera una planilla de alumnos en formato PDF la misma también posee una imagen.
- logo-colegio.png: Es una imagen que se colocará dentro del PDF, la misma esta dentro de la carpeta img

Autor
=====
- Matias Baez
- @matt_profe
- mbcorp.matias@gmail.com
- https://mattprofe.com.ar
